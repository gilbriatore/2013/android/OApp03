package br.up.edu.oapp03;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void somar(View v){

        EditText cx1 = (EditText) findViewById(R.id.txtNumero1);
        EditText cx2 = (EditText) findViewById(R.id.txtNumero2);
        EditText cxR = (EditText) findViewById(R.id.txtResultado);

        String txtCx1 = cx1.getText().toString();
        String txtCx2 = cx2.getText().toString();

        int vlr1 = Integer.parseInt(txtCx1);
        int vlr2 = Integer.parseInt(txtCx2);

        int resultado = vlr1 + vlr2;

        cxR.setText(String.valueOf(resultado));
    }

    public void subtrair(View v){

        EditText cx1 = (EditText) findViewById(R.id.txtNumero1);
        EditText cx2 = (EditText) findViewById(R.id.txtNumero2);
        EditText cxR = (EditText) findViewById(R.id.txtResultado);

        String txtCx1 = cx1.getText().toString();
        String txtCx2 = cx2.getText().toString();

        int vlr1 = Integer.parseInt(txtCx1);
        int vlr2 = Integer.parseInt(txtCx2);

        int resultado = vlr1 - vlr2;

        cxR.setText(String.valueOf(resultado));
    }

    public void multiplicar(View v){

        EditText cx1 = (EditText) findViewById(R.id.txtNumero1);
        EditText cx2 = (EditText) findViewById(R.id.txtNumero2);
        EditText cxR = (EditText) findViewById(R.id.txtResultado);

        String txtCx1 = cx1.getText().toString();
        String txtCx2 = cx2.getText().toString();

        int vlr1 = Integer.parseInt(txtCx1);
        int vlr2 = Integer.parseInt(txtCx2);

        int resultado = vlr1 * vlr2;

        cxR.setText(String.valueOf(resultado));
    }

    public void dividir(View v){

        EditText cx1 = (EditText) findViewById(R.id.txtNumero1);
        EditText cx2 = (EditText) findViewById(R.id.txtNumero2);
        EditText cxR = (EditText) findViewById(R.id.txtResultado);

        String txtCx1 = cx1.getText().toString();
        String txtCx2 = cx2.getText().toString();

        int vlr1 = Integer.parseInt(txtCx1);
        int vlr2 = Integer.parseInt(txtCx2);

        int resultado = vlr1 / vlr2;

        cxR.setText(String.valueOf(resultado));
    }



}
